const { NearProvider, nearlib } = require('near-web3-provider');
const web3 = require('web3');

// Configuration to run locally.
const TEST_NEAR_ACCOUNT = 'niketan_antier.testnet';
const testAccount = web3.utils.keccak256(TEST_NEAR_ACCOUNT).slice(26, 66);
const testNetworkId = 'testnet';

const keyPairString = 'ed25519:3papvpdyzJu4TomxTb1ADDseBnWbD197AX96SEfoZpgTAy16eVdUZCM7DPsxGZEK7gUxDDjEpnJJnv2Rz8rKRKbt';
const keyPair = nearlib.utils.KeyPair.fromString(keyPairString);
const testKeyStore = new nearlib.keyStores.InMemoryKeyStore();
testKeyStore.setKey(testNetworkId, TEST_NEAR_ACCOUNT, keyPair);

// Configuration for TestNet.
ACCOUNT_ID = 'ethdenverdemo'
const fileKeyStore = new nearlib.keyStores.UnencryptedFileSystemKeyStore('neardev');
const networkId = 'default';
const defaultAccount = web3.utils.keccak256(ACCOUNT_ID).slice(26, 66);


module.exports = {
  networks: {

    near_test: {
        network_id: '99',
        provider: function() {
            return new NearProvider('https://rpc.testnet.near.org.', testKeyStore, TEST_NEAR_ACCOUNT, testNetworkId);
        },
        from: testAccount,
        skipDryRun: true
    },

    development: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*' // Match any network id
    }
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: '0.5.12',    // Fetch exact version from solc-bin (default: truffle's version)
    }
  }
};
